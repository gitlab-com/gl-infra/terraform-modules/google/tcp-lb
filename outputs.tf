output "address" {
  description = "**DEPRECATED** Use `address_ipv4`, IP address of the load balancer."
  value       = local.is_external_lb ? google_compute_address.default[0].address : google_compute_forwarding_rule.internal[0].ip_address
}

output "address_ipv4" {
  description = "IPv4 address of the load balancer."
  value       = local.is_external_lb ? google_compute_address.default[0].address : google_compute_forwarding_rule.internal[0].ip_address
}

output "address_ipv6" {
  description = "IPv6 address of the load balancer."
  value       = local.is_external_lb ? (var.support_ipv6 ? google_compute_global_address.default_ipv6[0].address : null) : null
}
