# GitLab TCP Load Balancer Terraform Module

## What is this?

This module provisions a GCE TCP load balancer.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Troubleshooting

Sometimes (especially when bootstrapping a new `tcp-lb`) this module might have internal dependency conflits.
To resolve those you'll need to targeted apply this module in this order:

```shell
tf apply -target module.<module name>.google_compute_forwarding_rule.default
tf apply -target module.<module name>
```

After the above, a terraform plan for the entire module shall succeed.

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.3 |
| <a name="requirement_google"></a> [google](#requirement\_google) | >= 4.0.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_google"></a> [google](#provider\_google) | >= 4.0.0 |

## Modules

| Name | Source | Version |
|------|--------|---------|
| <a name="module_dns_record_external"></a> [dns\_record\_external](#module\_dns\_record\_external) | ops.gitlab.net/gitlab-com/dns-record/dns | 3.17.0 |
| <a name="module_dns_record_internal"></a> [dns\_record\_internal](#module\_dns\_record\_internal) | ops.gitlab.net/gitlab-com/dns-record/dns | 3.17.0 |

## Resources

| Name | Type |
|------|------|
| [google_compute_address.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_address) | resource |
| [google_compute_backend_service.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_backend_service) | resource |
| [google_compute_firewall.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_firewall) | resource |
| [google_compute_forwarding_rule.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_forwarding_rule) | resource |
| [google_compute_forwarding_rule.internal](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_forwarding_rule) | resource |
| [google_compute_global_address.default_ipv6](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_address) | resource |
| [google_compute_global_forwarding_rule.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_global_forwarding_rule) | resource |
| [google_compute_health_check.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_health_check) | resource |
| [google_compute_http_health_check.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_http_health_check) | resource |
| [google_compute_target_pool.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_target_pool) | resource |
| [google_compute_target_tcp_proxy.default](https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/compute_target_tcp_proxy) | resource |
| [google_compute_lb_ip_ranges.ranges](https://registry.terraform.io/providers/hashicorp/google/latest/docs/data-sources/compute_lb_ip_ranges) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_allow_global_access"></a> [allow\_global\_access](#input\_allow\_global\_access) | Allow access to the internal load balancer from all regions. | `bool` | `null` | no |
| <a name="input_backend_service"></a> [backend\_service](#input\_backend\_service) | BackendService to receive the matched traffic (internal or IPv6 load balancer only). | `string` | `""` | no |
| <a name="input_backend_service_protocol"></a> [backend\_service\_protocol](#input\_backend\_service\_protocol) | Protocol for the backend service (external IPv6 load balancer only) | `string` | `"TCP"` | no |
| <a name="input_backend_service_timeout_sec"></a> [backend\_service\_timeout\_sec](#input\_backend\_service\_timeout\_sec) | Timeout for connecting to the backend service (external IPv6 load balancer only) | `number` | `30` | no |
| <a name="input_environment"></a> [environment](#input\_environment) | The environment name. | `string` | n/a | yes |
| <a name="input_forwarding_port_ranges"></a> [forwarding\_port\_ranges](#input\_forwarding\_port\_ranges) | List of ports forwarded to the backend service. | `list(string)` | n/a | yes |
| <a name="input_fqdns"></a> [fqdns](#input\_fqdns) | List of FQDNs for this load balancer. | `list(string)` | `[]` | no |
| <a name="input_gitlab_zone"></a> [gitlab\_zone](#input\_gitlab\_zone) | DNS zone to create the FQDN records in. | `string` | `null` | no |
| <a name="input_health_check_ports"></a> [health\_check\_ports](#input\_health\_check\_ports) | List of health check ports (external load balancer only). | `list(string)` | `[]` | no |
| <a name="input_health_check_request_paths"></a> [health\_check\_request\_paths](#input\_health\_check\_request\_paths) | List of health check request paths, defaults to `/-/available-{name}` (external load balancer only). | `list(string)` | `[]` | no |
| <a name="input_instance_groups"></a> [instance\_groups](#input\_instance\_groups) | Instance groups for the backend service (external IPv6 load balancer only) | `list(string)` | `[]` | no |
| <a name="input_instances"></a> [instances](#input\_instances) | List of instances in the target pool (external load balancer only). | `list(string)` | n/a | yes |
| <a name="input_labels"></a> [labels](#input\_labels) | Labels to add to each of the managed resources. | `map(string)` | `{}` | no |
| <a name="input_lb_count"></a> [lb\_count](#input\_lb\_count) | Number of load balancers to create (external load balancer only). | `number` | n/a | yes |
| <a name="input_load_balancing_scheme"></a> [load\_balancing\_scheme](#input\_load\_balancing\_scheme) | What the load balancer forwarding will be used for, must be `EXTERNAL` or `INTERNAL`. | `string` | `"EXTERNAL"` | no |
| <a name="input_name"></a> [name](#input\_name) | Name of the load balancer. | `string` | n/a | yes |
| <a name="input_names"></a> [names](#input\_names) | Names for the forwarding rules (external load balancer only). | `list(string)` | n/a | yes |
| <a name="input_network"></a> [network](#input\_network) | The network that the load balanced IP should belong to for its forwarding rule, defaults to `environment` (internal load balancer only). | `string` | `null` | no |
| <a name="input_project"></a> [project](#input\_project) | The project name. | `string` | n/a | yes |
| <a name="input_proxied"></a> [proxied](#input\_proxied) | Whether the external load balancer FQDNs should be proxied by Cloudflare (external load balancer only). | `bool` | `null` | no |
| <a name="input_region"></a> [region](#input\_region) | The target region. | `string` | n/a | yes |
| <a name="input_session_affinity"></a> [session\_affinity](#input\_session\_affinity) | Target pool session affinity (external load balancer only). | `string` | `"NONE"` | no |
| <a name="input_spectrum_config"></a> [spectrum\_config](#input\_spectrum\_config) | Cloudflare Spectrum configuration for the external load balancer FQDNs (external load balancer only). | <pre>object({<br/>    ip_firewall    = optional(bool, true)<br/>    ip_override    = optional(list(string), [])<br/>    ports          = map(string)<br/>    proxy_protocol = optional(string, "off")<br/>  })</pre> | `null` | no |
| <a name="input_subnetwork_self_link"></a> [subnetwork\_self\_link](#input\_subnetwork\_self\_link) | The subnetwork that the load balanced IP should belong to for its forwarding rule (internal load balancer only). | `string` | `""` | no |
| <a name="input_support_ipv6"></a> [support\_ipv6](#input\_support\_ipv6) | Whether the LB supports IPv6 (external load balancer only) | `bool` | `false` | no |
| <a name="input_targets"></a> [targets](#input\_targets) | Target tags for the firewall rule. | `list(string)` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_address"></a> [address](#output\_address) | **DEPRECATED** Use `address_ipv4`, IP address of the load balancer. |
| <a name="output_address_ipv4"></a> [address\_ipv4](#output\_address\_ipv4) | IPv4 address of the load balancer. |
| <a name="output_address_ipv6"></a> [address\_ipv6](#output\_address\_ipv6) | IPv6 address of the load balancer. |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
