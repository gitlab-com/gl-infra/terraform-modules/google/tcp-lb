/*

TCP LBs are on the edge of our infrastructure and
represent critical components (including IP addresses)
that should never be destroyed or released
unless you are absolutely sure you know what you
are doing, for this reason we set a lifecycle
rule to prevent the destruction of these resources.

Note that variable interpolation does not work in
lifecycle attributes so this can not be parameterized.
See https://github.com/hashicorp/terraform/issues/3116

*/

resource "google_compute_forwarding_rule" "internal" {
  count = local.is_internal_lb ? 1 : 0

  name    = format("%v-%v", var.environment, var.name)
  project = var.project
  region  = var.region

  allow_global_access   = var.allow_global_access
  backend_service       = var.backend_service
  load_balancing_scheme = var.load_balancing_scheme
  network               = coalesce(var.network, var.environment)
  ports                 = var.forwarding_port_ranges
  service_label         = "i"
  subnetwork            = var.subnetwork_self_link

  labels = var.labels

  lifecycle {
    prevent_destroy = true
  }
}
