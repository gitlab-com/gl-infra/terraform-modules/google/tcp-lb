resource "google_compute_firewall" "default" {
  name    = format("%v-%v", var.environment, var.name)
  network = coalesce(var.network, var.environment)

  allow {
    protocol = "tcp"
    ports    = var.health_check_ports
  }

  source_ranges = concat(
    data.google_compute_lb_ip_ranges.ranges.network,
    data.google_compute_lb_ip_ranges.ranges.http_ssl_tcp_internal,
  )
  target_tags = var.targets
}
