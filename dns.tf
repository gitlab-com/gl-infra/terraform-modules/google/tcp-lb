module "dns_record_external" {
  source  = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "3.17.0"

  zone = var.gitlab_zone

  a = local.is_external_lb ? { for idx, fqdn in var.fqdns :
    fqdn => {
      records         = google_compute_address.default[*].address
      ttl             = 300
      proxied         = var.proxied
      spectrum_config = var.spectrum_config
    }
  } : {}

  aaaa = local.is_external_lb && var.support_ipv6 ? { for idx, fqdn in var.fqdns :
    fqdn => {
      records         = google_compute_global_address.default_ipv6[*].address
      ttl             = 300
      proxied         = var.proxied
      spectrum_config = var.spectrum_config
    }
  } : {}
}

module "dns_record_internal" {
  source  = "ops.gitlab.net/gitlab-com/dns-record/dns"
  version = "3.17.0"

  zone = var.gitlab_zone

  a = local.is_internal_lb ? { for idx, fqdn in var.fqdns :
    fqdn => {
      records = google_compute_forwarding_rule.internal[*].ip_address
      ttl     = 300
      # There is no spectrum config on internal loadbalancers, as that would not work.
    }
  } : {}
}
