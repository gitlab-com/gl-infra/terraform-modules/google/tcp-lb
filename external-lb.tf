/*

TCP LBs are on the edge of our infrastructure and
represent critical components (including IP addresses)
that should never be destroyed or released
unless you are absolutely sure you know what you
are doing, for this reason we set a lifecycle
rule to prevent the destruction of these resources.

Note that variable interpolation does not work in
lifecycle attributes so this can not be parameterized.
See https://github.com/hashicorp/terraform/issues/3116

*/

resource "google_compute_address" "default" {
  count = local.is_external_lb ? 1 : 0

  name    = format("%v-%v", var.environment, var.name)
  project = var.project
  region  = var.region

  address_type = var.load_balancing_scheme

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_compute_forwarding_rule" "default" {
  count = local.is_external_lb ? var.lb_count : 0

  name    = format("%v-%v-%v", var.environment, var.name, var.names[count.index])
  project = var.project
  region  = var.region

  ip_address            = google_compute_address.default[0].address
  load_balancing_scheme = var.load_balancing_scheme
  port_range            = var.forwarding_port_ranges[count.index]
  target                = google_compute_target_pool.default[count.index].self_link

  labels = var.labels

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_compute_target_pool" "default" {
  count = local.is_external_lb ? var.lb_count : 0

  name    = format("%v-%v-%v", var.environment, var.name, var.names[count.index])
  project = var.project
  region  = var.region

  session_affinity = var.session_affinity
  instances        = var.instances

  health_checks = [
    google_compute_http_health_check.default[count.index].self_link,
  ]

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_compute_http_health_check" "default" {
  count = local.is_external_lb ? var.lb_count : 0

  name    = format("%v-%v-%v", var.environment, var.name, var.names[count.index])
  project = var.project

  port                = var.health_check_ports[count.index]
  request_path        = element(coalescelist(var.health_check_request_paths, formatlist("/-/available-%v", var.names)), count.index)
  timeout_sec         = 2
  check_interval_sec  = 2
  healthy_threshold   = 2
  unhealthy_threshold = 2
}

resource "google_compute_global_address" "default_ipv6" {
  count = local.is_external_lb && var.support_ipv6 ? 1 : 0

  name    = format("%v-%v", var.environment, var.name)
  project = var.project

  address_type = var.load_balancing_scheme
  ip_version   = "IPV6"

  lifecycle {
    prevent_destroy = true
  }
}

resource "google_compute_backend_service" "default" {
  project = var.project
  count   = local.is_external_lb && var.support_ipv6 ? var.lb_count : 0

  name        = format("%v-%v-%v", var.environment, var.name, var.names[count.index])
  protocol    = var.backend_service_protocol
  port_name   = var.names[count.index]
  timeout_sec = var.backend_service_timeout_sec

  dynamic "backend" {
    for_each = var.instance_groups
    content {
      balancing_mode = "UTILIZATION"
      group          = var.instance_groups[backend.key]
    }
  }

  health_checks = [
    google_compute_health_check.default[count.index].self_link,
  ]
}

resource "google_compute_health_check" "default" {
  count = local.is_external_lb && var.support_ipv6 ? var.lb_count : 0

  name    = format("%v-%v-%v", var.environment, var.name, var.names[count.index])
  project = var.project

  http_health_check {
    port               = var.health_check_ports[count.index]
    request_path       = element(coalescelist(var.health_check_request_paths, formatlist("/-/available-%v", var.names)), count.index)
    port_specification = "USE_FIXED_PORT"
  }

  timeout_sec         = 2
  check_interval_sec  = 2
  healthy_threshold   = 2
  unhealthy_threshold = 2
}

resource "google_compute_target_tcp_proxy" "default" {
  count = local.is_external_lb && var.support_ipv6 ? var.lb_count : 0
  name  = format("%v-%v-%v", var.environment, var.name, var.names[count.index])

  backend_service = google_compute_backend_service.default[count.index].self_link
}

resource "google_compute_global_forwarding_rule" "default" {
  count = local.is_external_lb && var.support_ipv6 ? var.lb_count : 0

  name    = format("%v-%v-%v", var.environment, var.name, var.names[count.index])
  project = var.project

  ip_address            = google_compute_global_address.default_ipv6[0].address
  load_balancing_scheme = var.load_balancing_scheme
  port_range            = var.forwarding_port_ranges[count.index]
  target                = google_compute_target_tcp_proxy.default[count.index].self_link

  labels = var.labels

  lifecycle {
    prevent_destroy = true
  }
}