locals {
  is_internal_lb = var.load_balancing_scheme == "INTERNAL"
  is_external_lb = var.load_balancing_scheme == "EXTERNAL"
}
