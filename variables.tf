variable "lb_count" {
  type        = number
  description = "Number of load balancers to create (external load balancer only)."
}

variable "fqdns" {
  type        = list(string)
  description = "List of FQDNs for this load balancer."
  default     = []
}

variable "network" {
  type        = string
  description = "The network that the load balanced IP should belong to for its forwarding rule, defaults to `environment` (internal load balancer only)."
  default     = null
}

variable "subnetwork_self_link" {
  type        = string
  description = "The subnetwork that the load balanced IP should belong to for its forwarding rule (internal load balancer only)."
  default     = ""
}

variable "backend_service" {
  type        = string
  description = "BackendService to receive the matched traffic (internal or IPv6 load balancer only)."
  default     = ""
}

variable "spectrum_config" {
  type = object({
    ip_firewall    = optional(bool, true)
    ip_override    = optional(list(string), [])
    ports          = map(string)
    proxy_protocol = optional(string, "off")
  })
  description = "Cloudflare Spectrum configuration for the external load balancer FQDNs (external load balancer only)."
  default     = null
}

variable "proxied" {
  type        = bool
  description = "Whether the external load balancer FQDNs should be proxied by Cloudflare (external load balancer only)."
  default     = null
}

#####################################

variable "load_balancing_scheme" {
  type        = string
  description = "What the load balancer forwarding will be used for, must be `EXTERNAL` or `INTERNAL`."
  default     = "EXTERNAL"

  validation {
    condition     = contains(["EXTERNAL", "INTERNAL"], var.load_balancing_scheme)
    error_message = "The load_balancing_scheme value must be \"EXTERNAL\" or \"INTERNAL\"."
  }
}

variable "health_check_ports" {
  type        = list(string)
  description = "List of health check ports (external load balancer only)."
  default     = []
}

variable "health_check_request_paths" {
  type        = list(string)
  description = "List of health check request paths, defaults to `/-/available-{name}` (external load balancer only)."
  default     = []
}

variable "forwarding_port_ranges" {
  type        = list(string)
  description = "List of ports forwarded to the backend service."
}

variable "gitlab_zone" {
  type        = string
  description = "DNS zone to create the FQDN records in."
  default     = null
}

variable "instances" {
  type        = list(string)
  description = "List of instances in the target pool (external load balancer only)."
}

variable "targets" {
  type        = list(string)
  description = "Target tags for the firewall rule."
}

variable "environment" {
  type        = string
  description = "The environment name."
}

variable "name" {
  type        = string
  description = "Name of the load balancer."
}

variable "names" {
  type        = list(string)
  description = "Names for the forwarding rules (external load balancer only)."
}

variable "project" {
  type        = string
  description = "The project name."
}

variable "region" {
  type        = string
  description = "The target region."
}

variable "session_affinity" {
  type        = string
  description = "Target pool session affinity (external load balancer only)."
  default     = "NONE"
}

# Resource labels
variable "labels" {
  type        = map(string)
  description = "Labels to add to each of the managed resources."
  default     = {}
}

variable "allow_global_access" {
  type        = bool
  description = "Allow access to the internal load balancer from all regions."
  default     = null
}

variable "support_ipv6" {
  type        = bool
  description = "Whether the LB supports IPv6 (external load balancer only)"
  default     = false
}

variable "instance_groups" {
  type        = list(string)
  description = "Instance groups for the backend service (external IPv6 load balancer only)"
  default     = []
}

variable "backend_service_protocol" {
  type        = string
  description = "Protocol for the backend service (external IPv6 load balancer only)"
  default     = "TCP"
}

variable "backend_service_timeout_sec" {
  type        = number
  description = "Timeout for connecting to the backend service (external IPv6 load balancer only)"
  default     = 30
}